let poids = 100;
let taille = 1.75;

function decrire_corpulence(prmTaille, prmPoids){

    
    function calculerIMC(prmTaille,prmPoids) {
        let valIMC;
        valIMC = prmPoids / ( prmTaille * prmTaille);
        return valIMC;
    }

    let imc = calculerIMC(prmTaille,prmPoids);

    function interpreterIMC(prmIMC) {
        let interpretation = "";
        if (prmIMC < 16.5) {
            interpretation = 'Dénutrition';
        }else if(prmIMC >= 16.5 && prmIMC < 18.5){
            interpretation = 'Maigreur';
        }else if(prmIMC >= 18.5 && prmIMC < 25){
            interpretation = 'Corpulence normale';
        }else if(prmIMC >= 25 && prmIMC < 30){
            interpretation = 'Surpoids';
        }else if(prmIMC >= 30 && prmIMC < 35){
            interpretation = 'Obésité modérée';
        }else if(prmIMC >= 35 && prmIMC <= 40){
            interpretation = 'Obésité sévère';
        }else if(prmIMC > 40){
            interpretation = 'Obésité morbide';
        }
        return interpretation;
    }

    let inter = interpreterIMC(imc)

    let result = "votre IMC est égal à : " + imc + " , vous êtes en état de : " + inter;

    return result;
}

console.log("poids = " + poids + " kg");
console.log("taille = " + taille + " m");

let exit = decrire_corpulence(taille,poids);
console.log(exit)