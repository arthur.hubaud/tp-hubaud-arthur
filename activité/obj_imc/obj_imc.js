let objPatient = {
    nom : "Dupond",
    prenom : "Jean",
    age : 30,
    sexe : "masculin",
    taille : 180,
    poids : 85,
    decrire : function () {
        let description;
        description = this['prenom'] + " " + this['nom'] + " est agé de " + this['age'] + " ans, " + "il mesure "+this['taille']/100+" m et pèse "+this['poids']+" kg.";
        return description;
    },
    calculer_imc : function () {
        let imc;
        imc = this['poids'] / ( this['taille']/100 * this['taille']/100);
        return imc;
    },
    interpreter_IMC : function () {
        let interpretation = "";
        if (this.calculer_imc() < 16.5) {
            interpretation = 'Dénutrition';
        }else if(this.calculer_imc() >= 16.5 && this.calculer_imc() < 18.5){
            interpretation = 'Maigreur';
        }else if(this.calculer_imc() >= 18.5 && this.calculer_imc() < 25){
            interpretation = 'Corpulence normale';
        }else if(this.calculer_imc() >= 25 && this.calculer_imc() < 30){
            interpretation = 'Surpoids';
        }else if(this.calculer_imc() >= 30 && this.calculer_imc() < 35){
            interpretation = 'Obésité modérée';
        }else if(this.calculer_imc()>= 35 && this.calculer_imc() <= 40){
            interpretation = 'Obésité sévère';
        }else if(this.calculer_imc() > 40){
            interpretation = 'Obésité morbide';
        }
        return interpretation;
    },
    definir_corpulence : function () {
        let message = "";
        message = message.concat(this.decrire()) + '\n';
        message = message.concat("Son IMC est de " + this.calculer_imc())+ '\n';
        message = message.concat("il est en situation de " + this.interpreter_IMC())
     return message;  
    }
};

console.log(objPatient.definir_corpulence())