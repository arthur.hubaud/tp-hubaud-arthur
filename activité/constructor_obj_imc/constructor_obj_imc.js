


function patient(prmNom,prmPrenom,prmAge,prmSexe,prmTaille,prmPoids) {

    this.nom = prmNom;
    this.prenom = prmPrenom;
    this.age = prmAge;
    this.sexe = prmSexe;
    this.taille = prmTaille;
    this.poids = prmPoids;
    this.decrire = function () {
        let description;
        description = this['prenom'] + " " + this['nom'] + " est agé de " + this['age'] + " ans, " + "il mesure "+this['taille']/100+" m et pèse "+this['poids']+" kg.";
        return description;
    },
    this.calculer_imc = function () {
        let imc;
        imc = this['poids'] / ( this['taille']/100 * this['taille']/100);
        return imc;
    },
    this.interpreter_IMC = function () {
        let interpretation = "";

        if (this['sexe'] === "masculin") {
            if (this.calculer_imc() < 18.5) {
                interpretation = 'Dénutrition';
            }else if(this.calculer_imc() >= 18.5 && this.calculer_imc() < 20.5){
                interpretation = 'Maigreur';
            }else if(this.calculer_imc() >= 20.5 && this.calculer_imc() < 27){
                interpretation = 'Corpulence normale';
            }else if(this.calculer_imc() >= 27 && this.calculer_imc() < 32){
                interpretation = 'Surpoids';
            }else if(this.calculer_imc() >= 32 && this.calculer_imc() < 37){
                interpretation = 'Obésité modérée';
            }else if(this.calculer_imc()>= 37 && this.calculer_imc() <= 42){
                interpretation = 'Obésité sévère';
            }else if(this.calculer_imc() > 42){
                interpretation = 'Obésité morbide';
            }
        }else{
            if (this.calculer_imc() < 16.5) {
                interpretation = 'Dénutrition';
            }else if(this.calculer_imc() >= 16.5 && this.calculer_imc() < 18.5){
                interpretation = 'Maigreur';
            }else if(this.calculer_imc() >= 18.5 && this.calculer_imc() < 25){
                interpretation = 'Corpulence normale';
            }else if(this.calculer_imc() >= 25 && this.calculer_imc() < 30){
                interpretation = 'Surpoids';
            }else if(this.calculer_imc() >= 30 && this.calculer_imc() < 35){
                interpretation = 'Obésité modérée';
            }else if(this.calculer_imc()>= 35 && this.calculer_imc() <= 40){
                interpretation = 'Obésité sévère';
            }else if(this.calculer_imc() > 40){
                interpretation = 'Obésité morbide';
            }
        }
        
        return interpretation;
    },
    this.definir_corpulence = function () {
        let message = "";
        message = message.concat(this.decrire()) + '\n';
        message = message.concat("Son IMC est de " + this.calculer_imc())+ '\n';
        message = message.concat("il est en situation de " + this.interpreter_IMC())
     return message;  
    }

    
}


let objPatient1 = new patient('Hubaud','Arthur',20,"masculin",180,68);
let objPatient2 = new patient('Stark','Tony',53,"masculin",185,100);
let objPatient3 = new patient('Millie','Johnson',18,"feminin",163,44);

console.log(objPatient1.definir_corpulence())
console.log(objPatient2.definir_corpulence())
console.log(objPatient3.definir_corpulence())